<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220410172704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('0b811e9a-0f3f-4b89-ac21-b2ce811f69a4', 'Цветы', 'd7fed675-906f-4ac3-93aa-e37f027eafce');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('13413861-776b-4b2f-9130-a680f0410f05', 'Фрукты', 'd7fed675-906f-4ac3-93aa-e37f027eafce');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('224b03dc-72dc-4fac-abd9-3ab92c755500', 'Овощи', 'd7fed675-906f-4ac3-93aa-e37f027eafce');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('2660bd4e-ec14-4107-9317-8ef25803ed15', 'Шкура', '70bd138d-be0e-4479-9efb-35e9fa3932c5');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('543fefe0-24c8-4279-9982-022481014ca0', 'Мясо', '70bd138d-be0e-4479-9efb-35e9fa3932c5');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('70bd138d-be0e-4479-9efb-35e9fa3932c5', 'Дичь', NULL);
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('92511086-7b21-4d71-ae04-1aa3c0fbd454', 'Для дачи', 'd7fed675-906f-4ac3-93aa-e37f027eafce');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('94cf9b4b-b93e-48b3-9018-5b785224b2f3', 'Рыба', 'ad855494-0adf-4115-8fe4-3c30f1699121');
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('ad855494-0adf-4115-8fe4-3c30f1699121', 'Морепродукты', NULL);
INSERT INTO `announcement_categories` (`uuid`, `name`, `parent_uuid`) VALUES ('d7fed675-906f-4ac3-93aa-e37f027eafce', 'Хозяйство', NULL);
");
        $this->addSql("INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('022e654a-8dd5-4b4b-bcd6-64fc4bb4a2df', 'Помидоры', 'icons8-tomato-100.png');
INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('21bd2c9e-ca25-44cd-a638-7c0fb5956040', 'Свинина', 'icons8-pig-80.png');
INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('877fea32-0c1e-414e-bf70-901b3575b555', 'Рыба', 'icons8-fish-64.png');
INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('8addbdb8-535b-435f-8ff2-bb089e332f96', 'Говядина', 'icons8-cow-256.png');
INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('d6ad4a34-2612-4053-bda3-69da88227a84', 'Огурцы', 'icons8-cucumber-64.png');
INSERT INTO `announcement_groups` (`uuid`, `name`, `image`) VALUES ('e96d151b-59f2-44a2-931b-4e696322562e', 'Курица', 'icons8-chicken-80.png');
");
        $this->addSql("INSERT INTO `announcement_types` (`uuid`, `name`) VALUES ('bb1fe2f6-eb4e-43e7-94e8-972ea9557169', 'Продажа');
INSERT INTO `announcement_types` (`uuid`, `name`) VALUES ('d79aa7f0-a399-4642-8fb2-470d7d602bb7', 'Обмен');
INSERT INTO `announcement_types` (`uuid`, `name`) VALUES ('e38226b2-cb94-40af-8d40-1b50c894cffc', 'Купля');
");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE announcement_categories');
        $this->addSql('TRUNCATE announcement_groups');
        $this->addSql('TRUNCATE announcement_types');
    }
}
