<?php

namespace App\Common\DTO;

use App\Common\Domain\DTO\DTO;

class S3Object extends DTO
{
    public string $body;
    public string $contentType;
}
