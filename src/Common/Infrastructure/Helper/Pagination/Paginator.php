<?php

namespace App\Common\Infrastructure\Helper\Pagination;

use Doctrine\ORM\Query;

class Paginator
{
    private $query;

    private $pageSize;

    private $page;

    private $pageCount;

    private $results;

    public function __construct(Query $query, $page = 1, $pageSize = 20)
    {
        $this->query = $query;
        $this->pageSize = $pageSize;
        $this->page = $page;
    }

    public function make($mode = null)
    {
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($this->query);
        $totalItems = count($paginator);
        $this->pageCount = ceil($totalItems / $this->pageSize);

        $paginator->getQuery()
            ->setFirstResult($this->pageSize * ($this->page - 1)) // set the offset
            ->setMaxResults($this->pageSize);

        $this->results = $paginator->getQuery()->getResult($mode);
    }

    public function getResults()
    {
        return $this->results;
    }

    public function getPrevPages()
    {
        if ($this->page == 1) {
            return [];
        }

        $result = [];
        $page = $this->page;
        $pages = 0;

        while ($page != 1 && $pages != 3) {
            $pages++;
            $page--;
            $result[] = $page;
        }
        asort($result);
        return $result;
    }

    public function getNextPages()
    {
        if ($this->page == $this->pageCount) {
            return [];
        }

        $result = [];
        $page = $this->page;
        $pages = 0;

        while ($page != $this->pageCount && $pages != 3) {
            $pages++;
            $page++;
            $result[] = $page;
        }

        return $result;
    }

    public function getLastPage()
    {
        return $this->pageCount;
    }

    public function next()
    {
        if ($this->page == $this->pageCount) {
            return 0;
        }

        return $this->page + 1;
    }

    public function hasNext()
    {
        return $this->page < $this->pageCount;
    }

    public function hasLast()
    {
        if ($this->page == $this->pageCount) {
            return false;
        }

        if (!in_array($this->getLastPage(), $this->getNextPages())) {
            return true;
        }

        return false;
    }

    public function isEmpty(): bool
    {
        return !(bool)count($this->results);
    }
}