<?php

namespace App\Common\Infrastructure\Persistence\S3;

class FileNotFoundException extends \Exception
{
    protected $message = 'File not found';
}
