<?php

namespace App\Common\Infrastructure\Persistence\S3;

class FileUploadWasFailedException extends \Exception
{
    protected $message = 'File upload was failed';
}
