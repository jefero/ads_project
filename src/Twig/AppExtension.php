<?php

namespace App\Twig;

use App\Common\Infrastructure\Helper\DateHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('phone', [$this, 'formatPhone']),
            new TwigFilter('format_date', [$this, 'showDate']),
            new TwigFilter('declension', [$this, 'declension']),
        ];
    }

    public function formatPhone(string $phone): string
    {
        return substr($phone, 0, 2) . " (" . substr($phone, 2, 3) . ") " .
            substr($phone, 5, 3) . "-" . substr($phone, 8, 2) . "-" . substr($phone, 10, 2);
    }

    public function showDate(\DateTimeImmutable $date)
    {
        return DateHelper::createFromDate($date)->getFormattedDate();
    }

    public function declension(int $number, array $options): string
    {
        $array = [2, 0, 1, 1, 1, 2];
        return $options[($number % 100 > 4 && $number % 100 < 20) ? 2 : $array[($number % 10 < 5) ? $number % 10 : 5]];
    }
}