<?php

namespace App\Announcement\Application\Customer\Query\RemoveFavorite;

use App\Announcement\Domain\Customer\Entity\CustomerFavorite;
use App\Announcement\Domain\Customer\Service\CustomerRepository;
use App\Announcement\Domain\Customer\Service\FavoriteRepository;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Ramsey\Uuid\Uuid;

class RemoveFavoriteHandler
{
    private DoctrineRepository $doctrineRepository;
    private CustomerRepository $customerRepository;
    private FavoriteRepository $favoriteRepository;

    public function __construct(DoctrineRepository $doctrineRepository, CustomerRepository $customerRepository, FavoriteRepository $favoriteRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
        $this->customerRepository = $customerRepository;
        $this->favoriteRepository = $favoriteRepository;
    }

    public function handle(User $user, string $uuid)
    {
        $customer = $this->customerRepository->getByUserUuid($user->getUuid());

        $favorite = $this->favoriteRepository->findByCustomerAndAnnouncementUuid($customer->getUuid(), Uuid::fromString($uuid));

        if(!$favorite) {
            return;
        }

        $this->doctrineRepository->remove($favorite);
    }
}