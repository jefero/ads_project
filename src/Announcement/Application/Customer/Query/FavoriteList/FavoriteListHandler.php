<?php

namespace App\Announcement\Application\Customer\Query\FavoriteList;

use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Customer\Service\CustomerRepository;
use App\Announcement\Domain\Customer\Service\FavoriteRepository;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;

class FavoriteListHandler
{
    private DoctrineRepository $doctrineRepository;
    private CustomerRepository $customerRepository;
    private FavoriteRepository $favoriteRepository;
    private AnnouncementRepository $announcementRepository;

    public function __construct(
        DoctrineRepository $doctrineRepository,
        CustomerRepository $customerRepository,
        FavoriteRepository $favoriteRepository,
        AnnouncementRepository $announcementRepository
    ) {
        $this->doctrineRepository = $doctrineRepository;
        $this->customerRepository = $customerRepository;
        $this->favoriteRepository = $favoriteRepository;
        $this->announcementRepository = $announcementRepository;
    }

    public function handle(User $user)
    {
        $customer = $this->customerRepository->getByUserUuid($user->getUuid());
        return $this->announcementRepository->getFavorites($customer->getUuid());
    }
}