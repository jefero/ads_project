<?php


namespace App\Announcement\Application\Customer\Command\EditCustomer;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class EditCustomerCommand extends DTO
{
    /**
     * @OA\Property(property = "first_name", type = "string",
     *  example = "Title",
     * )
     *
     * @Assert\NotBlank
     */
    public string $first_name;

    /**
     * @OA\Property(property = "last_name", type = "string",
     *  example = "Title",
     * )
     *
     * @Assert\NotBlank
     */
    public string $last_name;

    /**
     * @OA\Property(property = "birth_date", type = "string",
     *  example = "2021-05-19T00:04:03+00:00",
     * )
     */
    public ?string $birth_date;
}