<?php


namespace App\Announcement\Application\Customer\Command\EditCustomer;


use App\Announcement\Domain\Customer\Service\CustomerRepository;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;

class EditCustomerHandler
{
    private DoctrineRepository $doctrineRepository;
    private CustomerRepository $customerRepository;

    public function __construct(DoctrineRepository $doctrineRepository, CustomerRepository $customerRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
        $this->customerRepository = $customerRepository;
    }

    public function handle(User $user, EditCustomerCommand $command): void
    {
        $customer = $this->customerRepository->getByUserUuid($user->getUuid());
        $customer->setFirstName($command->first_name);
        $customer->setLastName($command->last_name);

        if(!$customer->getBirthDate() && $command->birth_date) {
            $customer->setBirthDate(new \DateTimeImmutable($command->birth_date));
        }

        $this->doctrineRepository->save($customer);
    }
}