<?php

namespace App\Announcement\Application\Announcement\Command\SearchAnnouncement;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class SearchAnnouncementCommand extends DTO
{
    /**
     * @OA\Property(property = "limit", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $limit;
    /**
     * @OA\Property(property = "max_price", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $max_price = 0;

    /**
     * @OA\Property(property = "min_price", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $min_price = 0;

    /**
     * @OA\Property(property = "sort", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $sort;

    /**
     * @OA\Property(property = "page", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $page;

    /**
     * @OA\Property(property = "category", type = "integer",
     *  example = "Title",
     * )
     */
    public ?string $category = "";
}