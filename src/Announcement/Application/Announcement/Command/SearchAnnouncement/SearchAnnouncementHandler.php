<?php

namespace App\Announcement\Application\Announcement\Command\SearchAnnouncement;

use App\Announcement\Domain\Announcement\Entity\Announcement;
use App\Announcement\Domain\Announcement\Entity\Category;
use App\Common\Infrastructure\Helper\Pagination\Paginator;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;

class SearchAnnouncementHandler
{
    const SORT_RELEVANT = 1,
        SORT_PRICE = 2,
        SORT_NEWEST = 3;

    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
    }

    public function handle(SearchAnnouncementCommand $command)
    {
        $announcements = [];

        $qb = $this->doctrineRepository->getQueryBuilder()
            ->select("announcement, category.name")
            ->from(Announcement::class, "announcement")
            ->join(Category::class, "category", Join::WITH, "announcement.categoryUuid = category.uuid");

        if ($command->category) {
            $qb->leftJoin(Category::class, "parentCategory", Join::WITH, "category.parentUuid = parentCategory.uuid");
            $qb->andWhere("announcement.categoryUuid = :categoryUuid OR (parentCategory.uuid is not null AND parentCategory.uuid = :categoryUuid)")
                ->setParameter("categoryUuid", $command->category);
        }

        if ($command->max_price) {
            $qb->andWhere("announcement.price <= :maxPrice")
                ->setParameter("maxPrice", $command->max_price);
        }

        if ($command->min_price) {
            $qb->andWhere("announcement.price >= :minPrice")
                ->setParameter("minPrice", $command->min_price);
        }

        switch ($command->sort) {
            case self::SORT_RELEVANT: {
                $qb->orderBy("announcement.uuid", "DESC");
                break;
            }
            case self::SORT_PRICE: {
                $qb->orderBy("announcement.price", "ASC");
                break;
            }
            case self::SORT_NEWEST: {
                $qb->orderBy("announcement.createDate", "DESC");
                break;
            }
        }

        $paginator = new Paginator($qb->getQuery(), $command->page, $command->limit);
        $paginator->make(Query::HYDRATE_ARRAY);

        foreach ($paginator->getResults() as $item) {
            $announcements[] = array_merge($item[0], ["category" => $item["name"]]);
        }

        return [
            "result" => $announcements,
            "paginator" => $paginator
        ];
    }
}