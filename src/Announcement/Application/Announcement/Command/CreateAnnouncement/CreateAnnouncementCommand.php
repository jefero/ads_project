<?php

namespace App\Announcement\Application\Announcement\Command\CreateAnnouncement;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class CreateAnnouncementCommand extends DTO
{
    /**
     * @OA\Property(property = "title", type = "string",
     *  example = "Title",
     * )
     *
     * @Assert\NotBlank
     */
    public string $title;

    /**
     * @OA\Property(property = "type_id", type = "integer",
     *  example = "1",
     * )
     *
     * @Assert\NotBlank
     */
    public int $type_id;

    /**
     * @OA\Property(property = "category_uuid", type = "string",
     *  example = "1",
     * )
     *
     * @Assert\NotBlank
     */
    public string $category_uuid;

    /**
     * @OA\Property(property = "description", type = "string",
     *  example = "description",
     * )
     *
     * @Assert\NotBlank
     */
    public string $description;

    /**
     * @OA\Property(property = "images", type = "array",
     *  example = "description",
     * )
     *
     */
    public array $images = [];

    /**
     * @OA\Property(property = "address", type = "string",
     *  example = "description",
     * )
     *
     * @Assert\NotBlank
     */
    public string $address;

    /**
     * @OA\Property(property = "contact", type = "string",
     *  example = "79781126466",
     * )
     *
     */
    public ?string $contact;

    /**
     * @OA\Property(property = "contact", type = "integer",
     *  example = "79781126466",
     * )
     *
     * @Assert\NotBlank
     */
    public int $price;
}