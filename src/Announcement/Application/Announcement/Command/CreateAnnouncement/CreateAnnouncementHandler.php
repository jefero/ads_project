<?php

namespace App\Announcement\Application\Announcement\Command\CreateAnnouncement;

use App\Announcement\Domain\Announcement\Entity\Announcement;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Announcement\Domain\Announcement\Service\ImageRepository;
use App\Announcement\Domain\Customer\Entity\Customer;
use App\Announcement\Domain\Customer\Service\CustomerRepository;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Ramsey\Uuid\Uuid;

class CreateAnnouncementHandler
{
    private DoctrineRepository $doctrineRepository;
    private CategoryRepository $categoryRepository;
    private CustomerRepository $customerRepository;
    private ImageRepository $imageRepository;

    public function __construct(
        DoctrineRepository $doctrineRepository,
        CategoryRepository $categoryRepository,
        CustomerRepository $customerRepository,
        ImageRepository $imageRepository
    ) {
        $this->doctrineRepository = $doctrineRepository;
        $this->categoryRepository = $categoryRepository;
        $this->customerRepository = $customerRepository;
        $this->imageRepository = $imageRepository;
    }

    public function handle(User $user, CreateAnnouncementCommand $createAnnouncementCommand)
    {
        $customer = $this->customerRepository->getByUserUuid($user->getUuid());
        $category = $this->categoryRepository->get(Uuid::fromString($createAnnouncementCommand->category_uuid));
        $entities = [];

        $announcement = new Announcement(
            $customer->getUuid(),
            $createAnnouncementCommand->title,
            $createAnnouncementCommand->price,
            $createAnnouncementCommand->description,
            $category->getUuid(),
            $createAnnouncementCommand->type_id,
            $createAnnouncementCommand->address,
            $createAnnouncementCommand->contact,
        );
        $entities[] = $announcement;

        foreach ($createAnnouncementCommand->images as $imageUuid) {
            $image = $this->imageRepository->get(Uuid::fromString($imageUuid));
            $image->setAnnouncementUuid($announcement->getUuid());
            $entities[] = $image;
        }

        if(!empty($createAnnouncementCommand->images)) {
            $image = $this->imageRepository->get(Uuid::fromString(current($createAnnouncementCommand->images)));

            $announcement->setMainImageLink($image->getUrl());
        }

        $this->save(...$entities);
    }

    public function save(...$entities): void
    {
        $this->doctrineRepository->save(...$entities);
    }
}