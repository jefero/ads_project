<?php

namespace App\Announcement\Application\Announcement\Command\GetImage;

use App\Announcement\Domain\Announcement\Entity\Image;
use App\Common\DTO\S3Object;
use App\Common\Infrastructure\Persistence\S3\S3FileStorage;

class GetImageHandler
{
    private S3FileStorage $s3FileStorage;

    public function __construct(S3FileStorage $s3FileStorage)
    {
        $this->s3FileStorage = $s3FileStorage;
    }

    public function handle(string $path): S3Object
    {
        $bucket = Image::getBucket();

        return $this->s3FileStorage->getObject($bucket, $path);
    }
}