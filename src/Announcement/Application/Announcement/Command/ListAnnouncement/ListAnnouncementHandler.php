<?php

namespace App\Announcement\Application\Announcement\Command\ListAnnouncement;

use App\Announcement\Domain\Announcement\Entity\Announcement;
use App\Announcement\Domain\Announcement\Entity\Category;
use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;

class ListAnnouncementHandler
{
    private DoctrineRepository $doctrineRepository;
    private CategoryRepository $categoryRepository;

    public function __construct(
        DoctrineRepository $doctrineRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->doctrineRepository = $doctrineRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function handle(ListAnnouncementCommand $listAnnouncementCommand)
    {
        $announcements = [];

        $result = $this->doctrineRepository->getQueryBuilder()
            ->select("announcement, category.name")
            ->from(Announcement::class, "announcement")
            ->join(Category::class, "category", Join::WITH, "announcement.categoryUuid = category.uuid")
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        foreach ($result as $item) {
            $announcements[] = array_merge($item[0], ["category" => $item["name"]]);
        }

        return $announcements;
    }
}