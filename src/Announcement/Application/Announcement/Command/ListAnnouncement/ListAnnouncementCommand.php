<?php

namespace App\Announcement\Application\Announcement\Command\ListAnnouncement;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class ListAnnouncementCommand extends DTO
{
    /**
     * @OA\Property(property = "limit", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $limit;

    /**
     * @OA\Property(property = "sort", type = "integer",
     *  example = "Title",
     * )
     */
    public ?int $sort;
}