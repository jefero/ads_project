<?php

namespace App\Announcement\Application\Announcement\Command\UploadImage;

use App\Announcement\Domain\Profile\Entity\Image;
use App\Common\Infrastructure\Persistence\S3\S3FileStorage;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadImageHandler
{
    private S3FileStorage $s3FileStorage;

    private EntityManagerInterface $entityManager;

    public function __construct(S3FileStorage $s3FileStorage, EntityManagerInterface $entityManager)
    {
        $this->s3FileStorage = $s3FileStorage;
        $this->entityManager = $entityManager;
    }

    public function handle(UploadedFile $file): Image
    {
        $image = new Image();
        $image->setPath(Uuid::uuid4() . '.' . $file->guessExtension());

        $this->entityManager->persist($image);
        $this->entityManager->flush($image);

        return $image;
    }
}