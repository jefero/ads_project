<?php

namespace App\Announcement\Application\Announcement\Command\CreateCommentAnnouncement;

use App\Announcement\Domain\Announcement\Entity\Comment;
use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Announcement\Service\AnnouncementService;
use App\Announcement\Domain\Customer\Service\CustomerRepository;
use App\Announcement\Domain\Customer\Service\CustomerService;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Ramsey\Uuid\Uuid;

class CreateCommentAnnouncementHandler
{
    private DoctrineRepository $doctrineRepository;
    private AnnouncementRepository $announcementRepository;
    private CustomerRepository $customerRepository;
    private CustomerService $customerService;
    private AnnouncementService $announcementService;

    public function __construct(
        DoctrineRepository $doctrineRepository,
        AnnouncementRepository $announcementRepository,
        CustomerRepository $customerRepository,
        CustomerService $customerService,
        AnnouncementService $announcementService
    ) {
        $this->doctrineRepository = $doctrineRepository;
        $this->announcementRepository = $announcementRepository;
        $this->customerRepository = $customerRepository;
        $this->customerService = $customerService;
        $this->announcementService = $announcementService;
    }

    public function handle(User $user, CreateCommentAnnouncementCommand $createAnnouncementCommand)
    {
        $customer = $this->customerRepository->getByUserUuid($user->getUuid());
        $announcement = $this->announcementRepository->get(Uuid::fromString($createAnnouncementCommand->announcement_uuid));

        $comment = new Comment($announcement->getUuid(), $customer->getUuid(), $createAnnouncementCommand->text, $createAnnouncementCommand->rating);
        $this->save($comment);

        $this->announcementService->recalculateRating($announcement);
        $this->customerService->recalculateRating($customer);
    }

    public function save(...$entities): void
    {
        $this->doctrineRepository->save(...$entities);
    }
}