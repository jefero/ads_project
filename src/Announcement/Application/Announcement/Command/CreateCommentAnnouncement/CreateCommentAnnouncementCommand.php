<?php

namespace App\Announcement\Application\Announcement\Command\CreateCommentAnnouncement;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class CreateCommentAnnouncementCommand extends DTO
{
    /**
     * @OA\Property(property = "text", type = "string",
     *  example = "Title",
     * )
     *
     * @Assert\NotBlank
     */
    public string $text;

    /**
     * @OA\Property(property = "announcement_uuid", type = "string",
     *  example = "Title",
     * )
     *
     * @Assert\NotBlank
     */
    public string $announcement_uuid;

    /**
     * @OA\Property(property = "rating", type = "integer",
     *  example = "1",
     * )
     *
     * @Assert\NotBlank
     */
    public int $rating;
}