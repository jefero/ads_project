<?php


namespace App\Announcement\Domain\Customer\Service;

use App\Announcement\Domain\Customer\Entity\Customer;
use App\Announcement\Domain\Customer\Exception\NotFoundCustomerException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class CustomerRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Customer::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?Customer
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function getByUserUuid(UuidInterface $userUuid): ?Customer
    {
        $customer = $this->objectRepository->findOneBy(["userUuid" => $userUuid]);

        if(!$customer) {
            throw new NotFoundCustomerException();
        }

        return $customer;
    }
}