<?php


namespace App\Announcement\Domain\Customer\Service;

use App\Announcement\Domain\Customer\Entity\Customer;
use App\Announcement\Domain\Customer\Entity\CustomerFavorite;
use App\Announcement\Domain\Customer\Exception\NotFoundCustomerException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class FavoriteRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(CustomerFavorite::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?CustomerFavorite
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function findByCustomerAndAnnouncementUuid(UuidInterface $customerUuid, UuidInterface $announcementUuid): ?CustomerFavorite
    {
        return $this->objectRepository->findOneBy([
            "customerUuid" => $customerUuid,
            "announcementUuid" => $announcementUuid,
        ]);
    }

    public function findByCustomertUuid(UuidInterface $customerUuid)
    {
        return $this->objectRepository->findBy([
            "customerUuid" => $customerUuid
        ]);
    }
}