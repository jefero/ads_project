<?php

namespace App\Announcement\Domain\Customer\Service;

use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Customer\Entity\Customer;
use App\Common\Infrastructure\Persistence\DoctrineRepository;

class CustomerService
{
    private DoctrineRepository $doctrineRepository;
    private CustomerRepository $customerRepository;
    private AnnouncementRepository $announcementRepository;

    public function __construct(
        DoctrineRepository $doctrineRepository,
        AnnouncementRepository $announcementRepository,
        CustomerRepository $customerRepository
    ) {
        $this->doctrineRepository = $doctrineRepository;
        $this->customerRepository = $customerRepository;
        $this->announcementRepository = $announcementRepository;
    }

    public function recalculateRating(Customer $customer)
    {
        $announcements = $this->announcementRepository->findAllByCustomer($customer->getUuid());

        if(empty($announcements)) {
            return;
        }

        $allRating = 0.0;
        foreach ($announcements as $announcement) {
            $allRating += $announcement->getRating();
        }

        $customer->setRating($allRating / count($announcements));

        $this->doctrineRepository->save($customer);
    }
}