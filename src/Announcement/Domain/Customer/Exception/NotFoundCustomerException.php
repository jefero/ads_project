<?php

namespace App\Announcement\Domain\Customer\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class NotFoundCustomerException extends \DomainException
{
    protected $code = DomainExceptionCode::CUSTOMER_NOT_FOUND;
    protected $message = 'Пользователь не найден';
}