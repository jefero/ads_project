<?php

namespace App\Announcement\Domain\Customer\Entity;

use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="customer_favorites")
 */
class CustomerFavorite
{

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="customer_uuid", type="uuid", unique=true)
     */
    private UuidInterface $customerUuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="announcement_uuid", type="uuid", unique=true)
     */
    private UuidInterface $announcementUuid;

    public function __construct(UuidInterface $customerUuid, UuidInterface $announcementUuid)
    {
        $this->uuid = Uuid::uuid4();
        $this->customerUuid = $customerUuid;
        $this->announcementUuid = $announcementUuid;
    }
}