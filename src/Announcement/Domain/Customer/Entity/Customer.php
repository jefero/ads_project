<?php

declare(strict_types=1);

namespace App\Announcement\Domain\Customer\Entity;

use App\Common\Domain\DTO\ValueObject\Phone;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="customers")
 */
class Customer
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="user_uuid", type="uuid", unique=true)
     */
    private UuidInterface $userUuid;

    /**
     * @var string|null
     * @ORM\Column(name="first_name", type="string", nullable=true)
     */
    private ?string $firstName;

    /**
     * @var string|null
     * @ORM\Column(name="first_name", type="string", nullable=true)
     */
    private ?string $lastName;

    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(name="birth_date", type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $birthDate;

    /**
     * @var string|null
     * @ORM\Column(name="image_link", type="string", nullable=true)
     */
    private ?string $imageLink;

    /**
     * @var float
     * @ORM\Column(name="rating", type="float", nullable=false)
     */
    private float $rating;

    public function __construct(UuidInterface $userUuid)
    {
        $this->uuid = Uuid::uuid4();
        $this->userUuid = $userUuid;
        $this->rating = 0;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating(float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getBirthDate(): ?\DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTimeImmutable|null $birthDate
     */
    public function setBirthDate(?\DateTimeImmutable $birthDate): void
    {
        $this->birthDate = $birthDate;
    }
}