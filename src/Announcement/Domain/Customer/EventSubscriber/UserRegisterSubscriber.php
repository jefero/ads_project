<?php

namespace App\Announcement\Domain\Customer\EventSubscriber;

use App\Announcement\Domain\Customer\Entity\Customer;
use App\Auth\Domain\User\Event\UserRegisterEvent;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserRegisterSubscriber implements EventSubscriberInterface
{
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserRegisterEvent::NAME => [
                ["createCustomer"]
            ]
        ];
    }

    public function createCustomer(UserRegisterEvent $event)
    {
        $customer = new Customer($event->getUser()->getUuid());
        $this->doctrineRepository->save($customer);
    }
}