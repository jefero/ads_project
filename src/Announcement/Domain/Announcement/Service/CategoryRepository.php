<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\Category;
use App\Announcement\Domain\Announcement\Exception\NotFoundCategoryException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class CategoryRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Category::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?Category
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function get(UuidInterface $uuid): Category
    {
        $group = $this->objectRepository->findOneBy(["uuid" => $uuid]);
        if (!$group) {
            throw new NotFoundCategoryException();
        }
        return $group;
    }

    /**
     * @return Category[]
     */
    public function getNotParentCategories()
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("category")
            ->from(Category::class, "category")
            ->where("category.parentUuid is not null")
            ->getQuery()->getResult();
    }

    /**
     * @return Category[]
     */
    public function getParentCategories()
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("category")
            ->from(Category::class, "category")
            ->where("category.parentUuid is null")
            ->getQuery()->getResult();
    }

    public function getAvailableCategories(?string $categoryUuid)
    {
        if(!$categoryUuid) {
            return $this->getParentCategories();
        }

        return $this->doctrineRepository->getQueryBuilder()
            ->select("category")
            ->from(Category::class, "category")
            ->where("category.parentUuid = :categoryUuid")
            ->setParameter("categoryUuid", $categoryUuid)
            ->getQuery()->getResult();
    }
}