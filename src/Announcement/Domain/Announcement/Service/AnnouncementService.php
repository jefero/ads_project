<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\Announcement;
use App\Common\Infrastructure\Persistence\DoctrineRepository;

class AnnouncementService
{
    private DoctrineRepository $doctrineRepository;
    private AnnouncementRepository $announcementRepository;
    private CommentRepository $commentRepository;

    public function __construct(DoctrineRepository $doctrineRepository, AnnouncementRepository $announcementRepository, CommentRepository $commentRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
        $this->announcementRepository = $announcementRepository;
        $this->commentRepository = $commentRepository;

    }

    public function recalculateRating(Announcement $announcement)
    {
        $comments = $this->commentRepository->findAllByAnnouncement($announcement->getUuid());
        if(empty($comments)) {
            return;
        }
        $allRating = 0.0;
        foreach ($comments as $comment) {
            $allRating += $comment->getRating();
        }

        $announcement->setRating($allRating / count($comments));

        $this->doctrineRepository->save($announcement);
    }
}