<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\AnnouncementGroup;
use App\Announcement\Domain\Announcement\Exception\NotFoundAnnouncementException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class AnnouncementGroupRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(AnnouncementGroup::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?AnnouncementGroup
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function getMostPopular($limit = 6)
    {
        return $this->objectRepository->findBy([], [], $limit);
    }
}