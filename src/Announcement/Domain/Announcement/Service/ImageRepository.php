<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\Category;
use App\Announcement\Domain\Announcement\Entity\Image;
use App\Announcement\Domain\Announcement\Exception\NotFoundCategoryException;
use App\Announcement\Domain\Announcement\Exception\NotFoundImageException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class ImageRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Image::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?Image
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function findByAnnouncementUuid(UuidInterface $uuid): array
    {
        return $this->objectRepository->findBy(["announcementUuid" => $uuid]);
    }

    public function get(UuidInterface $uuid): Image
    {
        $image = $this->objectRepository->findOneBy(["uuid" => $uuid]);
        if (!$image) {
            throw new NotFoundImageException();
        }
        return $image;
    }
}