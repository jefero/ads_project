<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\Announcement;
use App\Announcement\Domain\Announcement\Entity\Category;
use App\Announcement\Domain\Announcement\Exception\NotFoundAnnouncementException;
use App\Announcement\Domain\Customer\Entity\CustomerFavorite;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class AnnouncementRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Announcement::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?Announcement
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    /**
     * @param UuidInterface $uuid
     * @return Announcement[]
     */
    public function findAllByCustomer(UuidInterface $uuid)
    {
        return $this->objectRepository->findBy(["customerUuid" => $uuid]);
    }

    public function get(UuidInterface $uuid): Announcement
    {
        $announcement = $this->objectRepository->findOneBy(["uuid" => $uuid]);
        if (!$announcement) {
            throw new NotFoundAnnouncementException();
        }
        return $announcement;
    }

    public function getMore(Announcement $announcement, $limit = 10)
    {
        $announcements = [];

        $result = $this->doctrineRepository->getQueryBuilder()
            ->select("announcement, category.name")
            ->from(Announcement::class, "announcement")
            ->join(Category::class, "category", Join::WITH, "announcement.categoryUuid = category.uuid")
            ->andWhere("announcement.categoryUuid = :categoryUuid")->setParameter("categoryUuid", $announcement->getCategoryUuid())
            ->setMaxResults($limit)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        foreach ($result as $item) {
            $announcements[] = array_merge($item[0], ["category" => $item["name"]]);
        }

        return $announcements;
    }

    public function getFavorites(UuidInterface $customerUuid)
    {
        $announcements = [];

        $result = $this->doctrineRepository->getQueryBuilder()
            ->select("announcement, category.name")
            ->from(Announcement::class, "announcement")
            ->join(Category::class, "category", Join::WITH, "announcement.categoryUuid = category.uuid")
            ->join(CustomerFavorite::class, "favorite", Join::WITH, "announcement.uuid = favorite.announcementUuid")
            ->andWhere("favorite.customerUuid = :customerUuid")->setParameter("customerUuid", $customerUuid)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        foreach ($result as $item) {
            $announcements[] = array_merge($item[0], ["category" => $item["name"]]);
        }

        return $announcements;
    }
}