<?php


namespace App\Announcement\Domain\Announcement\Service;


use App\Announcement\Domain\Announcement\Entity\Category;
use App\Announcement\Domain\Announcement\Entity\Comment;
use App\Announcement\Domain\Announcement\Exception\NotFoundCategoryException;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class CommentRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Comment::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?Comment
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    /**
     * @param UuidInterface $uuid
     * @return Comment[]
     */
    public function findAllByAnnouncement(UuidInterface $uuid)
    {
        return $this->objectRepository->findBy(["announcementUuid" => $uuid]);
    }

    public function get(UuidInterface $uuid): Comment
    {
        $group = $this->objectRepository->findOneBy(["uuid" => $uuid]);
        if (!$group) {
            throw new NotFoundCategoryException();
        }
        return $group;
    }
}