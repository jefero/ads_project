<?php

declare(strict_types=1);

namespace App\Announcement\Domain\Announcement\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="announcements")
 */
class Announcement
{
    const TYPE_ID_BUY = 1,
        TYPE_ID_SELL = 2,
        TYPE_ID_TRADE = 3,
        TYPE_UUID = [
            self::TYPE_ID_SELL => "bb1fe2f6-eb4e-43e7-94e8-972ea9557169",
            self::TYPE_ID_BUY => "e38226b2-cb94-40af-8d40-1b50c894cffc",
            self::TYPE_ID_TRADE => "d79aa7f0-a399-4642-8fb2-470d7d602bb7",
        ];

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="customer_uuid", type="uuid", nullable=false)
     */
    private UuidInterface $customerUuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="group_uuid", type="uuid", nullable=true)
     */
    private UuidInterface $groupUuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="category_uuid", type="uuid", nullable=false)
     */
    private UuidInterface $categoryUuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="type_uuid", type="uuid", nullable=false)
     */
    private UuidInterface $typeUuid;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private string $title;

    /**
     * @var int
     * @ORM\Column(name="price", type="integer")
     */
    private int $price;

    /**
     * @var string
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    private string $body;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", nullable=false)
     */
    private string $address;

    /**
     * @var string
     * @ORM\Column(name="contact", type="string", nullable=false)
     */
    private string $contact;

    /**
     * @var string|null
     * @ORM\Column(name="main_image_link", type="string", nullable=true)
     */
    private ?string $mainImageLink;

    /**
     * @var float
     * @ORM\Column(name="rating", type="float", nullable=false)
     */
    private float $rating;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="create_date", type="datetime_immutable", nullable=true)
     */
    private \DateTimeImmutable $createDate;

    public function __construct(
        UuidInterface $customerUuid,
        string $title,
        int $price,
        string $body,
        string $categoryUuid,
        int $typeId,
        string $address,
        ?string $contact
    ) {
        $this->uuid = Uuid::uuid4();
        $this->customerUuid = $customerUuid;
        $this->title = $title;
        $this->price = $price;
        $this->body = $body;
        $this->address = $address;
        $this->contact = $contact;
        $this->categoryUuid = Uuid::fromString($categoryUuid);
        $this->typeUuid = Uuid::fromString(self::TYPE_UUID[$typeId]);
        $this->createDate = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getMainImageLink(): ?string
    {
        return $this->mainImageLink;
    }

    /**
     * @param string|null $mainImageLink
     */
    public function setMainImageLink(?string $mainImageLink): void
    {
        $this->mainImageLink = $mainImageLink;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateDate(): \DateTimeImmutable
    {
        return $this->createDate;
    }

    /**
     * @return UuidInterface
     */
    public function getCustomerUuid(): UuidInterface
    {
        return $this->customerUuid;
    }

    /**
     * @return UuidInterface
     */
    public function getCategoryUuid(): UuidInterface
    {
        return $this->categoryUuid;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating(float $rating): void
    {
        $this->rating = $rating;
    }
}