<?php

declare(strict_types=1);

namespace App\Announcement\Domain\Announcement\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="announcement_comments")
 */
class Comment
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="announcement_uuid", type="uuid", nullable=true)
     */
    private UuidInterface $announcementUuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="customer_uuid", type="uuid", nullable=true)
     */
    private UuidInterface $customerUuid;

    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private string $text;

    /**
     * @var int
     * @ORM\Column(name="rating", type="integer")
     */
    private int $rating;

    public function __construct(UuidInterface $announcementUuid, UuidInterface $customerUuid, string $text, int $rating = 0)
    {
        $this->uuid = Uuid::uuid4();
        $this->announcementUuid = $announcementUuid;
        $this->customerUuid = $customerUuid;
        $this->text = $text;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }
}