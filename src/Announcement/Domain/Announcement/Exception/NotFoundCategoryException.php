<?php


namespace App\Announcement\Domain\Announcement\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class NotFoundCategoryException extends \DomainException
{
    protected $code = DomainExceptionCode::ANNOUNCEMENT_CATEGORY_NOT_FOUND;
    protected $message = 'Категория не найдена';
}