<?php


namespace App\Announcement\Domain\Announcement\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class NotFoundAnnouncementException extends \DomainException
{
    protected $code = DomainExceptionCode::PHONE_IS_NOT_VALID;
    protected $message = 'Номер телефона неверен';
}