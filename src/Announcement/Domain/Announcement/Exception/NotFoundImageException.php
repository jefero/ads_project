<?php


namespace App\Announcement\Domain\Announcement\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class NotFoundImageException extends \DomainException
{
    protected $code = DomainExceptionCode::ANNOUNCEMENT_IMAGE_NOT_FOUND;
    protected $message = 'Изображение не найдено';
}