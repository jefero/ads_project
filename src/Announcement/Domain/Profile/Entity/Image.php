<?php

namespace App\Announcement\Domain\Profile\Entity;

use App\Common\Domain\Entity\FileInfo;
use App\Common\Infrastructure\Persistence\S3\S3FileStorage;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="announcement_images")
 */
class Image implements FileInfo, \JsonSerializable
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var string
     * @ORM\Column(name="link", type="text")
     */
    private string $link;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public static function getBucket(): string
    {
        return S3FileStorage::BUCKET_AVATAR;
    }

    public function getPath(): string
    {
        return $this->link;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function setPath(string $fileName): void
    {
        $dirPath = (new \DateTimeImmutable())->format('Y/m/d') . '/';
        $this->link = $dirPath . $fileName;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'path' => $this->link,
            'url'  => $this->getUrl(),
        ];
    }

    public function getUrl(): string
    {
        return $this->link ? S3FileStorage::getAnnouncementFullUrl($this->link) : '';
    }
}