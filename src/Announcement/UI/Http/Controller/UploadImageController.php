<?php

namespace App\Announcement\UI\Http\Controller;

use App\Auth\Application\User\Command\Login\LoginCommand;
use App\Common\UI\Http\JsonApiController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class UploadImageController extends JsonApiController
{
    /**
     * @Route("/image/upload", name = "announcement_image_upload", methods = {"POST"})
     *
     * @Operation(
     *  tags={"Customer"},
     *  summary="Получение токена для пользователя по паролю",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          ref = @Model(type = LoginCommand::class),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true,
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null,
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        //TODO implement action
        return new Response();
    }
}