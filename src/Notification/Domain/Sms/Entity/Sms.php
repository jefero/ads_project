<?php

declare(strict_types=1);

namespace App\Notification\Domain\Sms\Entity;

use App\Common\Domain\DTO\ValueObject\Phone;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sms")
 */
class Sms
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var Phone
     * @ORM\Embedded(class="App\Common\Domain\DTO\ValueObject\Phone", columnPrefix=false)
     */
    private Phone $phone;

    /**
     * @var string
     * @ORM\Column(name="text", type="string", nullable=false)
     */
    private string $text;

    /**
     * @var bool
     * @ORM\Column(name="is_sent", type="boolean")
     */
    private bool $isSent = false;

    public function __construct(Phone $phone, string $text)
    {
        $this->uuid = Uuid::uuid4();
        $this->phone = $phone;
        $this->text = $text;
    }

    public function makeSent()
    {
        $this->isSent = true;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function isSent(): bool
    {
        return $this->isSent;
    }
}