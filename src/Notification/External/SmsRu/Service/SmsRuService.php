<?php


namespace App\Notification\External\SmsRu\Service;


use App\Common\Domain\DTO\ValueObject\Phone;
use App\Notification\Domain\Sms\Entity\Sms;

class SmsRuService
{
    const ROOT_URL = "https://sms.ru/sms/";
    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    private function sendRequest(string $method, $params)
    {
        $params = array_merge($params, [
            "api_id" => $this->apiKey,
            "json" => 1
        ]);

        $ch = curl_init(self::ROOT_URL . $method . "?" . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        return json_decode(curl_exec($ch));
    }

    public function send(Sms $sms): Sms
    {
        $response = $this->sendRequest("send", [
            "to" => $sms->getPhone()->getPhone(),
            "msg" => $sms->getText()
        ]);

        if ($response->status == "OK") {
            $sms->makeSent();
        }

        return $sms;
    }

    public function status(Sms $sms): bool
    {
        $response = $this->sendRequest("status", [
            "sms_id" => $sms->getText()
        ]);

        if ($response->status == "OK") {
            $sms->makeSent();
            return true;
        }

        return false;
    }
}