<?php

namespace App\Auth\Domain\User\Service;

use App\Announcement\Domain\Customer\Entity\Customer;
use App\Auth\Domain\User\Entity\ConfirmationCode;
use App\Auth\Domain\User\Entity\User;
use App\Auth\Domain\User\Event\UserRegisterEvent;
use App\Auth\Domain\User\Exception\CodeIsNotEqualException;
use App\Auth\Domain\User\Exception\CodeIsNotSentException;
use App\Auth\Domain\User\Exception\ConfirmationCodeAttemptsExceededException;
use App\Auth\Domain\User\Exception\PhoneIsNotValidException;
use App\Auth\Domain\User\Exception\TooManySmsException;
use App\Auth\Domain\User\Exception\UserAlreadyRegisteredException;
use App\Auth\Domain\User\Exception\UserNotRegisteredException;
use App\Auth\Domain\User\Exception\WrongPasswordException;
use App\Common\Domain\DTO\ValueObject\Phone;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use App\Notification\Domain\Sms\Entity\Sms;
use App\Notification\External\SmsRu\Service\SmsRuService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AuthService
{
    const TEST_PIN_CODE = "5555",
        TEST_PHONES = ["79781126467"];
    private DoctrineRepository $doctrineRepository;
    private EventDispatcherInterface $eventDispatcher;
    private UserRepository $userRepository;
    private ConfirmationCodeRepository $confirmationCodeRepository;
    private SmsRuService $smsRuService;
    private string $codeActiveInterval;
    private string $smsRequestInterval;
    protected int $attemptLoginCount;

    public function __construct(
        DoctrineRepository         $doctrineRepository,
        EventDispatcherInterface   $eventDispatcher,
        UserRepository             $userRepository,
        ConfirmationCodeRepository $confirmationCodeRepository,
        SmsRuService               $smsRuService,
        string                     $codeActiveInterval,
        string                     $smsRequestInterval,
        int                        $attemptLoginCount
    )
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->codeActiveInterval = $codeActiveInterval ?: "PT10M";
        $this->smsRequestInterval = $smsRequestInterval ?: "PT10M";
        $this->doctrineRepository = $doctrineRepository;
        $this->userRepository = $userRepository;
        $this->smsRuService = $smsRuService;
        $this->confirmationCodeRepository = $confirmationCodeRepository;
        $this->attemptLoginCount = $attemptLoginCount;
    }

    public function sendCode(string $phone, string $countryCode): bool
    {
        $phone = new Phone($phone, $countryCode);

        if (!$phone->validate()) {
            throw new PhoneIsNotValidException();
        }

        $user = $this->userRepository->findOneByPhone($phone);

        if ($user) {
            throw new UserAlreadyRegisteredException();
        }

        $this->cleanConfirmationCodes($phone);

        $pinCode = $this->isTestPhone($phone) ? self::TEST_PIN_CODE : (string)random_int(1111, 9999);

        $code = $this->confirmationCodeRepository->findOneActiveByPhone($phone);

        if (!$code) {
            $code = new ConfirmationCode($phone, $pinCode, $this->codeActiveInterval);
        }

        if (!$this->canSend($code)) {
            throw new TooManySmsException();
        }

        $code->setCode($pinCode);
        $code->update($this->codeActiveInterval);

        if (!$this->isTestPhone($phone)) {
            $sms = $this->smsRuService->send(new Sms(
                $phone,
                "Код подтверждения: $pinCode"
            ));

            $this->doctrineRepository->persist($sms);
            $smsResponse = $sms->isSent();
        } else {
            $smsResponse = true;
        }

        $this->doctrineRepository->persist($code);
        $this->doctrineRepository->flush();

        return $smsResponse;
    }

    public function cleanConfirmationCodes(Phone $phone)
    {
        $codes = $this->confirmationCodeRepository->findExpiredByPhone($phone);

        foreach ($codes as $code) {
            $this->doctrineRepository->remove($code);
        }
    }

    public function isTestPhone(Phone $phone): bool
    {
        return in_array($phone->getPhone(), self::TEST_PHONES);
    }

    public function canSend(ConfirmationCode $code): bool
    {
        $sentAt = $code->getSentAt();

        if (!$sentAt) {
            return true;
        }

        $allowDate = $sentAt->add(new \DateInterval($this->smsRequestInterval));

        return $allowDate < new \DateTimeImmutable();
    }

    public function register(string $phone, string $countryCode, string $pinCode, string $password): User
    {
        $phone = new Phone($phone, $countryCode);

        if (!$phone->validate()) {
            throw new PhoneIsNotValidException();
        }

        $user = $this->userRepository->findOneByPhone($phone);

        if ($user) {
            throw new UserAlreadyRegisteredException();
        }

        $this->cleanConfirmationCodes($phone);

        $code = $this->confirmationCodeRepository->findOneActiveByPhone($phone);

        if (!$code) {
            throw new CodeIsNotSentException();
        }

        if (!$this->canTry($code)) {
            throw new ConfirmationCodeAttemptsExceededException();
        }

        if (!$code->equal($pinCode)) {
            $code->addAttempt();

            $this->doctrineRepository->persist($code);
            $this->doctrineRepository->flush();
            throw new CodeIsNotEqualException();
        }

        $user = new User($phone, $password);

        $this->eventDispatcher->dispatch(new UserRegisterEvent($user), UserRegisterEvent::NAME);
        $this->doctrineRepository->persist($user);

        $this->doctrineRepository->flush();

        return $user;
    }

    private function canTry(ConfirmationCode $confirmationCode): bool
    {
        return $confirmationCode->getAttempt() != $this->attemptLoginCount;
    }

    public function login(string $phone, string $countryCode, string $password): User
    {
        $phone = new Phone($phone, $countryCode);

        if (!$phone->validate()) {
            throw new PhoneIsNotValidException();
        }

        $user = $this->userRepository->findOneByPhone($phone);

        if (!$user) {
            throw new UserNotRegisteredException();
        }

        if (!password_verify($password, $user->getPassword())) {
            throw new WrongPasswordException();
        }

        $user->login();

        $this->doctrineRepository->persist($user);
        $this->doctrineRepository->flush();

        return $user;
    }
}