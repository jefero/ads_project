<?php

namespace App\Auth\Domain\User\Service;

use App\Auth\Domain\User\Entity\ConfirmationCode;
use App\Common\Domain\DTO\ValueObject\Phone;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class ConfirmationCodeRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(ConfirmationCode::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function findByPhone(Phone $phone): ?ConfirmationCode
    {
        return $this->objectRepository->findBy(["phone" => $phone]);
    }

    public function findOneByPhone(Phone $phone): ?ConfirmationCode
    {
        return $this->objectRepository->findOneBy(["phone" => $phone]);
    }

    /**
     * @param Phone $phone
     * @return ConfirmationCode[]
     */
    public function findExpiredByPhone(Phone $phone): array
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("cc")
            ->from(ConfirmationCode::class, "cc")
            ->where("cc.valid < :valid")
            ->andWhere("cc.phone.phone = :phone")
            ->setParameter("valid", new \DateTimeImmutable())
            ->setParameter("phone", $phone->getPhone())
            ->getQuery()->getResult();
    }

    public function findOneActiveByPhone(Phone $phone): ?ConfirmationCode
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("cc")
            ->from(ConfirmationCode::class, "cc")
            ->where("cc.valid > :valid")
            ->andWhere("cc.phone.phone = :phone")
            ->setParameter("valid", new \DateTimeImmutable())
            ->setParameter("phone", $phone->getPhone())
            ->getQuery()->getOneOrNullResult();
    }
}