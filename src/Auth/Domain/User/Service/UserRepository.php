<?php

namespace App\Auth\Domain\User\Service;

use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\DTO\ValueObject\Phone;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class UserRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(User::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?User
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function findOneByPhone(Phone $phone): ?User
    {
        return $this->objectRepository->findOneBy([
            "phone.phone" => $phone->getPhone()
        ]);
    }
}