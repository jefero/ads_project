<?php

namespace App\Auth\Domain\User\Entity;

use App\Common\Domain\DTO\ValueObject\Phone;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="confirmation_codes")
 */
class ConfirmationCode
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var Phone
     * @ORM\Embedded(class="App\Common\Domain\DTO\ValueObject\Phone", columnPrefix=false)
     */
    private Phone $phone;

    /**
     * @var string
     * @ORM\Column(name="code", type="string")
     */
    private string $code;

    /**
     * @var int
     * @ORM\Column(name="attempt", type="string")
     */
    private int $attempt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="valid", type="datetime_immutable")
     */
    private \DateTimeImmutable $valid;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="sent_at", type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $sentAt;

    public function __construct(Phone $phone, string $code, string $codeActiveInterval)
    {
        $this->uuid = Uuid::uuid4();
        $this->phone = $phone;
        $this->code = $code;
        $this->attempt = 0;
        $this->sentAt = null;
        $this->valid = (new \DateTimeImmutable())->add(new \DateInterval($codeActiveInterval));
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getSentAt(): ?\DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function equal(string $code): bool
    {
        return $this->code == $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function update(string $codeActiveInterval): void
    {
        $this->valid = (new \DateTimeImmutable())->add(new \DateInterval($codeActiveInterval));
        $this->sentAt = new \DateTimeImmutable();
        $this->attempt = 0;
    }

    public function addAttempt(): void
    {
        $this->attempt++;
    }

    /**
     * @return int
     */
    public function getAttempt(): int
    {
        return $this->attempt;
    }
}