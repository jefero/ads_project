<?php

namespace App\Auth\Domain\User\Entity;

use App\Common\Domain\DTO\ValueObject\Phone;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var Phone
     * @ORM\Embedded(class="App\Common\Domain\DTO\ValueObject\Phone", columnPrefix=false)
     */
    private Phone $phone;

    /**
     * @var string
     * @ORM\Column(name="password", type="string")
     */
    private string $password;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="last_login", type="datetime_immutable", nullable=true)
     */
    private \DateTimeImmutable $lastLogin;

    public function __construct(Phone $phone, string $password)
    {
        $this->uuid = Uuid::uuid4();
        $this->phone = $phone;
        $this->password = password_hash($password, PASSWORD_BCRYPT, ["cost" => 12]);
    }

    public function getRoles(): array
    {
        return ["ROLE_USER"];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    public function getUsername(): string
    {
        return $this->phone->getPhone();
    }

    public function setPassword(string $password)
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT, ["cost" => 12]);
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function login()
    {
        $this->lastLogin = new \DateTimeImmutable();
    }

    public function supportsClass($class): bool
    {
        return $class === User::class;
    }
}