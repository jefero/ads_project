<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class UserNotRegisteredException extends \DomainException
{
    protected $code = DomainExceptionCode::USER_NOT_EXIST;
    protected $message = 'Пользователь не зарегистрирован';
}