<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class PhoneIsNotValidException extends \DomainException
{
    protected $code = DomainExceptionCode::ANNOUNCEMENT_NOT_FOUND;
    protected $message = 'Объявление не найдено';
}