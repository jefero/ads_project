<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class TooManySmsException extends \DomainException
{
    protected $code = DomainExceptionCode::TOO_MANY_SMS_REQUESTS;
    protected $message = 'Превышен лимит отправки смс';
}