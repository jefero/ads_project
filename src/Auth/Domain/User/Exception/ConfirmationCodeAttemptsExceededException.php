<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class ConfirmationCodeAttemptsExceededException extends \DomainException
{
    protected $code = DomainExceptionCode::CONFIRMATION_CODE_ATTEMPTS_EXCEEDED;
    protected $message = 'Превышено количество попыток проверки кода';
}