<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class UserAlreadyRegisteredException extends \DomainException
{
    protected $code = DomainExceptionCode::USER_ALREADY_EXIST;
    protected $message = 'Пользователь уже зарегистрирован';
}