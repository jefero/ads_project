<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class CodeIsNotSentException extends \DomainException
{
    protected $code = DomainExceptionCode::CODE_IS_NOT_SENT;
    protected $message = 'Не найден действующий код';
}