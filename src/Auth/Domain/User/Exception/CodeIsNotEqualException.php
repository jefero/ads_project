<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class CodeIsNotEqualException extends \DomainException
{
    protected $code = DomainExceptionCode::CODE_IS_NOT_EQUAL;
    protected $message = 'Код не соответсвует отправленному';
}