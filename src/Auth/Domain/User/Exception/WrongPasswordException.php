<?php

namespace App\Auth\Domain\User\Exception;

use App\Common\Domain\Exception\DomainExceptionCode;

class WrongPasswordException extends \DomainException
{
    protected $code = DomainExceptionCode::WRONG_PASSWORD;
    protected $message = 'Неверный пароль';
}