<?php

namespace App\Auth\Domain\Session\Entity;


use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\Entity\TimestampableImmutableTrait;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;
use DateTimeImmutable;

/**
 * @ORM\Entity()
 * @ORM\Table(name="auth_sessions")
 */
class Session
{
    use TimestampableImmutableTrait;

    public const REFRESH_TOKEN_NAME = 'refresh_token';

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="user_uuid", type="uuid")
     */
    private UuidInterface $userUuid;

    /**
     * @var string
     * @ORM\Column(name="refresh_token", type="string", nullable=true)
     */
    private ?string $refreshToken;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="finished_at", type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $finishedAt;

    public function __construct(User $user)
    {
        $this->userUuid = $user->getUuid();
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return UuidInterface
     */
    public function getUserUuid(): UuidInterface
    {
        return $this->userUuid;
    }

    /**
     * @param string|null $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    public function finish(): void
    {
        $this->finishedAt = new DateTimeImmutable();
    }
}
