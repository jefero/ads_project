<?php

declare(strict_types=1);

namespace App\Auth\Domain\Session\Service;


use App\Auth\Domain\Session\Entity\Session;
use App\Auth\Domain\User\Entity\User;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class SessionRepository
{
    private DoctrineRepository $doctrineRepository;
    private ObjectRepository $objectRepository;
    private bool $sessionSingle;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->doctrineRepository = $doctrineRepository;
        $this->objectRepository = $doctrineRepository->getObjectRepository(Session::class);
        $this->sessionSingle = true;
    }

    public function findOneActiveSession(UuidInterface $uuid): ?Session
    {
        /** @var Session $session */
        $session = $this->objectRepository->findOneBy(
            [
                'uuid'       => $uuid,
                'finishedAt' => null
            ]
        );

        return $session;
    }

    public function findActiveSessionsByUserUuid(UuidInterface $userUuid): array
    {
        /** @var Session[] $session */
        $sessions = $this->objectRepository->findBy(
            [
                'userUuid'   => $userUuid,
                'finishedAt' => null
            ]
        );

        return $sessions;
    }

    public function findOneActiveSessionByRefreshToken(string $refreshToken): ?Session
    {
        /** @var Session $session */
        $session = $this->objectRepository->findOneBy(
            [
                'refreshToken' => $refreshToken,
                'finishedAt'   => null
            ]
        );

        return $session;
    }

    public function createSession(User $user): Session {
        if ($this->sessionSingle) {
            $this->finishSessionsByUserUuid($user->getUuid());
        }

        $session = new Session($user);
        $this->save($session);

        return $session;
    }

    public function save(Session ...$sessions): void
    {
        $this->doctrineRepository->save(...$sessions);
    }

    private function finishSessionsByUserUuid(UuidInterface $userUuid): void
    {
        $sessions = $this->findActiveSessionsByUserUuid($userUuid);

        if (!empty($sessions)) {
            /** @var Session $session */
            foreach ($sessions as $session) {
                $session->finish();
            }

            $this->save(...$sessions);
        }
    }
}