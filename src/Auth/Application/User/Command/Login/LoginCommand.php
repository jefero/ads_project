<?php

namespace App\Auth\Application\User\Command\Login;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class LoginCommand extends DTO
{
    /**
     * @OA\Property(property = "phone", type = "string",
     *  example = "9781126466",
     * )
     *
     * @Assert\NotBlank
     */
    public string $phone;

    /**
     * @OA\Property(property = "phone", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $countryCode;

    /**
     * @OA\Property(property = "password", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $password;
}