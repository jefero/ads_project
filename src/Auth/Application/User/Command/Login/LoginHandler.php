<?php

namespace App\Auth\Application\User\Command\Login;

use App\Auth\Application\User\Command\Register\RegisterCommand;
use App\Auth\Domain\Session\Service\SessionRepository;
use App\Auth\Domain\User\Service\AuthService;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;

class LoginHandler
{
    private AuthService $authService;
    private AuthenticationSuccessHandler $authenticationSuccessHandler;
    private SessionRepository $sessionRepository;

    public function __construct(AuthService $authService, AuthenticationSuccessHandler $authenticationSuccessHandler, SessionRepository $sessionRepository)
    {
        $this->authService = $authService;
        $this->authenticationSuccessHandler = $authenticationSuccessHandler;
        $this->sessionRepository = $sessionRepository;
    }

    public function handle(Request $request, LoginCommand $loginHandler)
    {
        $user = $this->authService->login($loginHandler->phone, $loginHandler->countryCode, $loginHandler->password);
        $session = $this->sessionRepository->createSession($user);
        $request->request->set('sessionUuid', $session->getUuid()->toString());

        return $this->authenticationSuccessHandler->handleAuthenticationSuccess($user);
    }
}