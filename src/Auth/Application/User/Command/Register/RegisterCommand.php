<?php

namespace App\Auth\Application\User\Command\Register;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class RegisterCommand extends DTO
{
    /**
     * @OA\Property(property = "phone", type = "string",
     *  example = "9781126466",
     * )
     *
     * @Assert\NotBlank
     */
    public string $phone;

    /**
     * @OA\Property(property = "country_code", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $country_code;

    /**
     * @OA\Property(property = "pin_code", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $pin_code;

    /**
     * @OA\Property(property = "password", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $password;
}