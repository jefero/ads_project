<?php

namespace App\Auth\Application\User\Command\Register;

use App\Auth\Domain\Session\Service\SessionRepository;
use App\Auth\Domain\User\Entity\User;
use App\Auth\Domain\User\Exception\PhoneIsNotValidException;
use App\Auth\Domain\User\Service\AuthService;
use App\Auth\Domain\User\Service\ConfirmationCodeRepository;
use App\Auth\Domain\User\Service\UserRepository;
use App\Common\Domain\DTO\ValueObject\Phone;
use App\Notification\External\SmsRu\Service\SmsRuService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;

class RegisterHandler
{
    private AuthService $authService;
    private AuthenticationSuccessHandler $authenticationSuccessHandler;
    private SessionRepository $sessionRepository;

    public function __construct(AuthService $authService, AuthenticationSuccessHandler $authenticationSuccessHandler, SessionRepository $sessionRepository)
    {
        $this->authService = $authService;
        $this->authenticationSuccessHandler = $authenticationSuccessHandler;
        $this->sessionRepository = $sessionRepository;
    }

    public function handle(Request $request, RegisterCommand $registerCommand)
    {
        $user = $this->authService->register($registerCommand->phone,
            $registerCommand->country_code,
            $registerCommand->pin_code,
            $registerCommand->password
        );
        $session = $this->sessionRepository->createSession($user);
        $request->request->set('sessionUuid', $session->getUuid()->toString());

        return $this->authenticationSuccessHandler->handleAuthenticationSuccess($user);
    }
}