<?php

namespace App\Auth\Application\User\Command\SendPin;

use App\Auth\Domain\User\Entity\User;
use App\Auth\Domain\User\Exception\PhoneIsNotValidException;
use App\Auth\Domain\User\Service\AuthService;
use App\Auth\Domain\User\Service\ConfirmationCodeRepository;
use App\Auth\Domain\User\Service\UserRepository;
use App\Common\Domain\DTO\ValueObject\Phone;
use App\Notification\External\SmsRu\Service\SmsRuService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class SendPinHandler
{
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function handle(SendPinCommand $sendPinCommand)
    {
        $this->authService->sendCode($sendPinCommand->phone, $sendPinCommand->country_code);
    }
}