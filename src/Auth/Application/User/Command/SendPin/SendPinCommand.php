<?php

namespace App\Auth\Application\User\Command\SendPin;

use App\Common\Domain\DTO\DTO;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class SendPinCommand extends DTO
{
    /**
     * @OA\Property(property = "phone", type = "string",
     *  example = "9781126466",
     * )
     *
     * @Assert\NotBlank
     */
    public string $phone;

    /**
     * @OA\Property(property = "phone", type = "string",
     *  example = "RU",
     * )
     *
     * @Assert\NotBlank
     */
    public string $country_code;
}