<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller\User;

use App\Auth\Application\User\Command\SendPin\SendPinCommand;
use App\Auth\Application\User\Command\SendPin\SendPinHandler;
use App\Common\Domain\DTO\DTOFactory;
use App\Common\UI\Http\JsonApiController;
use App\Common\UI\Http\JsonApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;


class SendPinController extends JsonApiController
{
    private SendPinHandler $sendPinHandler;

    public function __construct(SendPinHandler $sendPinHandler)
    {
        $this->sendPinHandler = $sendPinHandler;
    }

    /**
     * @Route("/pin/send", name = "auth_user_send_pin", methods = {"POST"})
     *
     * @Operation(
     *  tags={"Customer"},
     *  summary="Отправка пин кода на номер",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          ref = @Model(type = SendPinCommand::class),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true,
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null,
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return JsonApiResponse
     */
    public function __invoke(Request $request): JsonApiResponse
    {
        $command = DTOFactory::createDtoFromRequest(SendPinCommand::class, $request);

        $this->sendPinHandler->handle($command);

        return JsonApiResponse::success();
    }
}