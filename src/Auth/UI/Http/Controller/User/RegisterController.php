<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller\User;

use App\Auth\Application\User\Command\Register\RegisterHandler;
use App\Auth\Application\User\Command\Register\RegisterCommand;
use App\Common\Domain\DTO\DTOFactory;
use App\Common\UI\Http\JsonApiController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;


class RegisterController extends JsonApiController
{
    private RegisterHandler $registerHandler;

    public function __construct(RegisterHandler $registerHandler)
    {
        $this->registerHandler = $registerHandler;
    }

    /**
     * @Route("/register", name = "auth_user_register", methods = {"POST"})
     *
     * @Operation(
     *  tags={"Customer"},
     *  summary="Регистрация пользователя",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          ref = @Model(type = RegisterCommand::class),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true,
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null,
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $command = DTOFactory::createDtoFromRequest(RegisterCommand::class, $request);

        return $this->registerHandler->handle($request, $command);
    }
}