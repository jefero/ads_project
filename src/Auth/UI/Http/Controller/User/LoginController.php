<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller\User;

use App\Auth\Application\User\Command\Login\LoginCommand;
use App\Auth\Application\User\Command\Login\LoginHandler;
use App\Common\Domain\DTO\DTOFactory;
use App\Common\UI\Http\JsonApiController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;


class LoginController extends JsonApiController
{
    private LoginHandler $loginHandler;

    public function __construct(LoginHandler $loginHandler)
    {
        $this->loginHandler = $loginHandler;
    }

    /**
     * @Route("/login", name = "auth_user_login", methods = {"POST"})
     *
     * @Operation(
     *  tags={"Customer"},
     *  summary="Получение токена для пользователя по паролю",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          ref = @Model(type = LoginCommand::class),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true,
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null,
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $command = DTOFactory::createDtoFromRequest(LoginCommand::class, $request);

        return $this->loginHandler->handle($request, $command);
    }
}