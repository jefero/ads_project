<?php

namespace App\Blog\Domain\News\Service;

use App\Blog\Domain\News\Entity\News;
use App\Common\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class NewsRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(News::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function find(UuidInterface $uuid): ?News
    {
        return $this->objectRepository->findOneBy(["uuid" => $uuid]);
    }

    public function findLatest($limit = 3)
    {
        return $this->objectRepository->findBy([], [
            "createDate" => "DESC"
        ], $limit);
    }
}