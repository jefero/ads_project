<?php

declare(strict_types=1);

namespace App\Blog\Domain\News\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    private string $title;

    /**
     * @var string
     * @ORM\Column(name="body", type="text")
     */
    private string $body;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="create_date", type="datetime_immutable", nullable=true)
     */
    private \DateTimeImmutable $createDate;

    public function __construct(string $title, string $body)
    {
        $this->uuid = Uuid::uuid4();
        $this->title = $title;
        $this->body = $body;
        $this->createDate = new \DateTimeImmutable();
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateDate(): \DateTimeImmutable
    {
        return $this->createDate;
    }
}