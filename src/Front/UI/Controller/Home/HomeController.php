<?php

namespace App\Front\UI\Controller\Home;

use App\Announcement\Application\Announcement\Command\ListAnnouncement\ListAnnouncementCommand;
use App\Announcement\Application\Announcement\Command\ListAnnouncement\ListAnnouncementHandler;
use App\Announcement\Domain\Announcement\Entity\AnnouncementGroup;
use App\Announcement\Domain\Announcement\Service\AnnouncementGroupRepository;
use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Blog\Domain\News\Service\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private NewsRepository $newsRepository;
    private AnnouncementGroupRepository $announcementGroupRepository;
    private ListAnnouncementHandler $listAnnouncementHandler;

    public function __construct(
        NewsRepository $newsRepository,
        AnnouncementGroupRepository $announcementGroupRepository,
        ListAnnouncementHandler $listAnnouncementHandler
    ) {
        $this->newsRepository = $newsRepository;
        $this->announcementGroupRepository = $announcementGroupRepository;
        $this->listAnnouncementHandler = $listAnnouncementHandler;
    }

    /**
     * @Route("/", name = "front_home", methods = {"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $groups = $this->announcementGroupRepository->getMostPopular();
        $newsList = $this->newsRepository->findLatest();
        $announcements = $this->listAnnouncementHandler->handle(new ListAnnouncementCommand([]));
        return $this->render("home.html.twig", [
            "newsList" => $newsList,
            "groups" => $groups,
            "announcements" => $announcements
        ]);
    }
}