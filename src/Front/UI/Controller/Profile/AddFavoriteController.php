<?php

namespace App\Front\UI\Controller\Profile;

use App\Announcement\Application\Customer\Command\EditCustomer\EditCustomerCommand;
use App\Announcement\Application\Customer\Command\EditCustomer\EditCustomerHandler;
use App\Announcement\Application\Customer\Query\AddFavorite\AddFavoriteHandler;
use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddFavoriteController extends AbstractController
{
    private AddFavoriteHandler $handler;

    public function __construct(AddFavoriteHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/favorite/add/{id}", name = "front_favorite_add", methods = {"GET"}, , requirements={"id"=".+"})
     *
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $this->handler->handle($user, $id);

        return $this->redirectToRoute("front_announcement_show", ["id" => $id]);
    }
}