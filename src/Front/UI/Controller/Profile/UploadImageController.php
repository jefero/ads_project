<?php

namespace App\Front\UI\Controller\Profile;

use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementHandler;
use App\Announcement\Application\Announcement\Command\UploadImage\UploadImageHandler;
use App\Auth\Domain\User\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadImageController extends AbstractController
{
    private UploadImageHandler $uploadImageHandler;

    public function __construct(UploadImageHandler $uploadImageHandler)
    {
        $this->uploadImageHandler = $uploadImageHandler;
    }

    /**
     * @Route("/profile/image/upload", name = "front_profile_image_upload", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $fileObject = $this->uploadImageHandler->handle($request->files->get("image"));
        return new Response(json_encode([
            "uuid" => $fileObject->getUuid()
        ]));
    }
}