<?php

namespace App\Front\UI\Controller\Profile;

use App\Announcement\Application\Customer\Query\FavoriteList\FavoriteListHandler;
use App\Auth\Domain\User\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileFavoritesController extends AbstractController
{
    private FavoriteListHandler $handler;

    public function __construct(FavoriteListHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/profile/favorites", name = "front_profile_favorites", methods = {"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $favorites = $this->handler->handle($user);
        return $this->render("Profile/profile.html.twig", [
            "favorites" => $favorites
        ]);
    }
}