<?php

namespace App\Front\UI\Controller\Profile;

use App\Announcement\Application\Customer\Command\EditCustomer\EditCustomerCommand;
use App\Announcement\Application\Customer\Command\EditCustomer\EditCustomerHandler;
use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileEditController extends AbstractController
{
    private EditCustomerHandler $handler;

    public function __construct(EditCustomerHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/profile/edit", name = "front_profile_edit", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if($request->getMethod() == Request::METHOD_POST) {
            $command = DTOFactory::createDtoFromRequest(EditCustomerCommand::class, $request);
            $this->handler->handle($user, $command);

            return $this->redirectToRoute("front_profile_index");
        }
        return $this->render("Profile/profile.html.twig");
    }
}