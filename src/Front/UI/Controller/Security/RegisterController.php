<?php

namespace App\Front\UI\Controller\Security;

use App\Auth\Application\User\Command\Register\RegisterCommand;
use App\Auth\Application\User\Command\Register\RegisterHandler;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private RegisterHandler $registerHandler;

    public function __construct(RegisterHandler $registerHandler)
    {
        $this->registerHandler = $registerHandler;
    }

    /**
     * @Route("/register", name = "front_security_register", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        if($this->getUser()) {
            return $this->redirectToRoute("front_home");
        }

        /** @var Session $session */
        $session = $request->getSession();

        if($request->getMethod() == Request::METHOD_POST) {
            $command = DTOFactory::createDtoFromRequest(RegisterCommand::class, $request);
            $command->phone = substr($command->phone, 2);

            try {
                $this->registerHandler->handle($request, $command);
            } catch (\Exception $exception) {
                $session->getFlashBag()->set("error", $exception->getMessage());
                return $this->redirectToRoute("front_security_register", [
                    "phone" => $request->request->get("phone")
                ]);
            }
            return $this->redirectToRoute("front_home");
        }

        $error = $session->getFlashBag()->has("error") ? current($session->getFlashBag()->get("error")) : null;
        $session->getFlashBag()->clear();

        return $this->render("Security/register.html.twig", [
            "error" => $error
        ]);
    }
}