<?php

namespace App\Front\UI\Controller\Security;

use App\Auth\Application\User\Command\SendPin\SendPinCommand;
use App\Auth\Application\User\Command\SendPin\SendPinHandler;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class SendCodeController extends AbstractController
{
    private SendPinHandler $sendPinHandler;

    public function __construct(SendPinHandler $sendPinHandler)
    {
        $this->sendPinHandler = $sendPinHandler;
    }

    /**
     * @Route("/send-code", name = "front_security_send_code", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        if($this->getUser()) {
            return $this->redirectToRoute("front_home");
        }

        /** @var Session $session */
        $session = $request->getSession();

        if($request->getMethod() == Request::METHOD_POST) {
            $command = DTOFactory::createDtoFromRequest(SendPinCommand::class, $request);
            $command->phone = substr($command->phone, 2);

            try {
                $this->sendPinHandler->handle($command);
            } catch (\Exception $exception) {
                $session->getFlashBag()->set("error", $exception->getMessage());
                return $this->redirectToRoute("front_security_send_code");
            }
            return $this->redirectToRoute("front_security_register", [
                "phone" => $request->request->get("phone")
            ]);
        }

        $error = $session->getFlashBag()->has("error") ? current($session->getFlashBag()->get("error")) : null;
        $session->getFlashBag()->clear();

        return $this->render("Security/send_code.html.twig", [
            "error" => $error
        ]);
    }
}