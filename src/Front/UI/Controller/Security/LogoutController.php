<?php

namespace App\Front\UI\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LogoutController extends AbstractController
{
    /**
     * @Route("/logout", name = "front_security_logout", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function __invoke(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        return $this->redirectToRoute("front_home");
    }
}