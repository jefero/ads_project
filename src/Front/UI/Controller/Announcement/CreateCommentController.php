<?php

namespace App\Front\UI\Controller\Announcement;

use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementCommand;
use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementHandler;
use App\Announcement\Application\Announcement\Command\CreateCommentAnnouncement\CreateCommentAnnouncementCommand;
use App\Announcement\Application\Announcement\Command\CreateCommentAnnouncement\CreateCommentAnnouncementHandler;
use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Auth\Application\User\Command\Login\LoginCommand;
use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateCommentController extends AbstractController
{
    private CreateCommentAnnouncementHandler $createCommentAnnouncementHandler;

    public function __construct(CreateCommentAnnouncementHandler $createCommentAnnouncementHandler)
    {
        $this->createCommentAnnouncementHandler = $createCommentAnnouncementHandler;
    }

    /**
     * @Route("/announcement/comment/create", name = "front_announcement_comment_create", methods = {"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $request->request->set("price", (int)$request->request->get("price"));
        $request->request->set("type_id", (int)$request->request->get("type_id"));

        $command = DTOFactory::createDtoFromRequest(CreateCommentAnnouncementCommand::class, $request);
        $this->createCommentAnnouncementHandler->handle($user, $command);

        return $this->redirectToRoute("front_announcement_show", [
            "id" => $command->announcement_uuid
        ]);

    }
}