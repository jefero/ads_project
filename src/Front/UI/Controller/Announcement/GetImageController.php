<?php

namespace App\Front\UI\Controller\Announcement;

use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementHandler;
use App\Announcement\Application\Announcement\Command\GetImage\GetImageHandler;
use App\Announcement\Application\Announcement\Command\UploadImage\UploadImageHandler;
use App\Auth\Domain\User\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetImageController extends AbstractController
{
    private GetImageHandler $getImageHandler;

    public function __construct(GetImageHandler $getImageHandler)
    {
        $this->getImageHandler = $getImageHandler;
    }

    /**
     * @Route("/announcement/image/get/{id}", name = "front_announcement_image_get", methods = {"GET", "POST"}, requirements={"id"=".+"})
     *
     * @param string $id
     * @return Response
     */
    public function __invoke(string $id): Response
    {
        $fileObject = $this->getImageHandler->handle($id);
        return new Response(
            $fileObject->body,
            Response::HTTP_OK,
            [
                'Content-Type' => $fileObject->contentType,
                'Cache-Control' => 'max-age=31536000',
            ]
        );
    }
}