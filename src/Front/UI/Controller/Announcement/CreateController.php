<?php

namespace App\Front\UI\Controller\Announcement;

use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementCommand;
use App\Announcement\Application\Announcement\Command\CreateAnnouncement\CreateAnnouncementHandler;
use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Auth\Application\User\Command\Login\LoginCommand;
use App\Auth\Domain\User\Entity\User;
use App\Common\Domain\DTO\DTOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    private CreateAnnouncementHandler $createAnnouncementHandler;
    private CategoryRepository $categoryRepository;

    public function __construct(CreateAnnouncementHandler $createAnnouncementHandler, CategoryRepository $categoryRepository)
    {
        $this->createAnnouncementHandler = $createAnnouncementHandler;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/announcement/create", name = "front_announcement_create", methods = {"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $categories = $this->categoryRepository->getNotParentCategories();
        if ($request->getMethod() == Request::METHOD_POST) {

            $request->request->set("price", (int)$request->request->get("price"));
            $request->request->set("type_id", (int)$request->request->get("type_id"));

            $command = DTOFactory::createDtoFromRequest(CreateAnnouncementCommand::class, $request);
            $this->createAnnouncementHandler->handle($user, $command);

            return $this->redirectToRoute("front_home");
        }

        return $this->render("Announcement/add.html.twig", [
            "categories" => $categories
        ]);
    }
}