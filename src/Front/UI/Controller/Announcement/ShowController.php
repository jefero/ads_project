<?php

namespace App\Front\UI\Controller\Announcement;

use App\Announcement\Domain\Announcement\Service\AnnouncementRepository;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Announcement\Domain\Announcement\Service\ImageRepository;
use App\Announcement\Domain\Customer\Service\CustomerRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowController extends AbstractController
{
    private AnnouncementRepository $announcementRepository;
    private CategoryRepository $categoryRepository;
    private CustomerRepository $customerRepository;
    private ImageRepository $imageRepository;

    public function __construct(
        AnnouncementRepository $announcementRepository,
        CategoryRepository $categoryRepository,
        CustomerRepository $customerRepository,
        ImageRepository $imageRepository
    ) {
        $this->announcementRepository = $announcementRepository;
        $this->categoryRepository = $categoryRepository;
        $this->customerRepository = $customerRepository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @Route("/announcement/show/{id}", name = "front_announcement_show", methods = {"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request, $id): Response
    {
        $announcement = $this->announcementRepository->get(Uuid::fromString($id));
        $customer = $this->customerRepository->find($announcement->getCustomerUuid());
        $images = $this->imageRepository->findByAnnouncementUuid($announcement->getUuid());

        return $this->render("Announcement/show.html.twig", [
            "announcement" => $announcement,
            "customer" => $customer,
            "relatedAnnouncements" => $this->announcementRepository->getMore($announcement),
            "currentCategory" => $this->categoryRepository->get($announcement->getCategoryUuid()),
            "images" => $images
        ]);
    }
}