<?php

namespace App\Front\UI\Controller\Announcement;

use App\Announcement\Application\Announcement\Command\SearchAnnouncement\SearchAnnouncementCommand;
use App\Announcement\Application\Announcement\Command\SearchAnnouncement\SearchAnnouncementHandler;
use App\Announcement\Domain\Announcement\Service\CategoryRepository;
use App\Common\Domain\DTO\DTOFactory;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilterController extends AbstractController
{
    private CategoryRepository $categoryRepository;

    private SearchAnnouncementHandler $searchAnnouncementHandler;

    public function __construct(CategoryRepository $categoryRepository, SearchAnnouncementHandler $searchAnnouncementHandler)
    {
        $this->categoryRepository = $categoryRepository;
        $this->searchAnnouncementHandler = $searchAnnouncementHandler;
    }

    /**
     * @Route("/announcement/filter/", name = "front_announcement_filter", methods = {"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $request->query->set("limit", 25);
        $request->query->set("sort", $request->query->get("sort", SearchAnnouncementHandler::SORT_RELEVANT));
        $request->query->set("page", $request->query->get("page", 1));
        $command = DTOFactory::createDtoFromQuery(SearchAnnouncementCommand::class, $request);
        $result = $this->searchAnnouncementHandler->handle($command);

        $request->query->remove("limit");

        return $this->render("Announcement/filter.html.twig", array_merge([
            "categories" => $this->categoryRepository->getAvailableCategories($request->query->get("category")),
            "currentCategory" => $command->category ? $this->categoryRepository->find(Uuid::fromString($command->category)) : null
        ], $result));
    }
}